﻿using apitest.Dtos.Character;
using apitest.Models;
using AutoMapper;

namespace apitest
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Character, GetCharacterDto>();
            CreateMap<AddCharacterDto, Character>();
        }
    }
}
